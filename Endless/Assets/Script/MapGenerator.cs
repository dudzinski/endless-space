﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour {

    //public Dictionary<int, Dictionary<int, GameObject>> tiles = new Dictionary<int, Dictionary<int, GameObject>>();
    public List<Tile> tiles = new List<Tile>();
    public Transform target;

    [Header("Config")]
    public float tilesSize = 15f;
    public int renderDistansX = 5;
    public int renderDistansY = 3;
    public GameObject pref;



    // Update is called once per frame
    /*void Updates ()
    {
        int x;
        int z;
        Vector3 newPos = target.position / 30;
        if (newPos.x <= 0)
            x = Mathf.CeilToInt(newPos.x);
        else
            x = Mathf.FloorToInt(newPos.x);

        z = Mathf.FloorToInt(newPos.z);

        Debug.Log(tiles.Count);

        for (int i = 0; i < renderDistansX; i++)
        {
            for (int j = -renderDistansY + 1; j < renderDistansY; j++)
            {
                if (!tiles.ContainsKey(j + x))
                {
                    GameObject newTiles = Instantiate(pref, new Vector3((j + x) * tilesSize * 2, 0, (i + z) * tilesSize * 2), Quaternion.identity);
                    newTiles.name = "Chunk " + (i + z) + " " + (j + x);
                    newTiles.AddComponent<DestroyTile>();
                    tiles.Add(j + x, new Dictionary<int, GameObject>());
                    tiles[j + x].Add(i + z, newTiles);
                }
                else
                {
                    if (!tiles[j + x].ContainsKey(i + z))
                    {
                        GameObject newTiles = Instantiate(pref, new Vector3((j + x) * tilesSize * 2, 0, (i + z) * tilesSize * 2), Quaternion.identity);
                        newTiles.name = "Chunk " + (i + z) + " " + (j + x);
                        newTiles.AddComponent<DestroyTile>();

                        tiles[j + x].Add(i + z, newTiles);
                    }
                }
            }
        }
	}
    */
    private void Update()
    {
        //Debug.Log(Application.targetFrameRate);
        int PosX = 0;
        int PosZ = 0;

        if (target.position.x <= 0)
        {
            PosX = Mathf.CeilToInt(target.position.x / 30);
        }
        else
        {
            PosX = Mathf.FloorToInt(target.position.x / 30);
        }
        PosZ = Mathf.FloorToInt(target.position.z / 30);

        bool exist = false;

        for (int x = -renderDistansX + 1; x < renderDistansX; x++)
        {
            for (int z = 0; z < renderDistansY; z++) //
            {
                tiles.ForEach(delegate (Tile item)
                {
                    if (item.x == x + PosX && item.z == z + PosZ)
                    {
                        exist = true;
                    }
                });

                if (!exist)
                {
                    //Debug.Log(x + " | " + z);
                    Vector3 newPos = new Vector3((x + PosX) * tilesSize, 0, (z + PosZ) * tilesSize);

                    GameObject newTile = Instantiate(pref, newPos, Quaternion.identity);
                    newTile.name = "Tile " + (x + PosX) + " | " + (z + PosZ);
                    newTile.AddComponent<DestroyTile>();

                    tiles.Add(new Tile(x + PosX, z + PosZ, newTile));
                }
                exist = false;

            }
        }
    }
}

public class Tile
{
    public int x;
    public int z;
    public GameObject obj;

    public Tile(int x, int z, GameObject obj)
    {
        this.x = x;
        this.z = z;
        this.obj = obj;
    }
}