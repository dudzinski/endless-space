﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Endless;

public class PlayerMovement : MonoBehaviour {


    public float maxRotation;
    public float speedRotation;
    public float speed;
    public float speedRotationa;

    public float eee;
    public float timer;

    void Start () {
        Camera.main.GetComponent<CameraController>().enabled = true;
        Application.targetFrameRate = 300;
	}
	
    private void FixedUpdate()
    {
        float direction = (float)InputManager.GetDirection();
        eee = Mathf.Lerp(eee, direction, 2 * Time.deltaTime);
        transform.localEulerAngles = new Vector3(0, 0, Mathf.LerpAngle(transform.localEulerAngles.z, maxRotation * -direction, speedRotation * Time.deltaTime));
        transform.position += new Vector3(speedRotationa * eee * Time.deltaTime, 0, speed * Time.deltaTime);
    }
}
